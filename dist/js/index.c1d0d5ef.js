import{d as V,r as p,a as o,o as y,c as S,w as n,b as e,e as c,f as B,g as C,F,h as j,t as k,i as q,u as z,j as L,k as T,l as x,m as U,H,M}from"./index.e04da2f0.js";import{s as I}from"./index.5548e431.js";import{_ as P}from"./_plugin-vue_export-helper.5059d46b.js";const $="/vue3-tiger-admin/svg/logo.da9b9095.svg",Q="/vue3-tiger-admin/png/login-code.8c841f1c.png",G={id:"jinrishici-sentence"},J=V({__name:"index",setup(g){const t=p(),l=()=>{const r="https://v2.jinrishici.com/one.json?client=browser-sdk/1.2?t="+new Date().getTime();q.get(r).then(d=>{I(t,d.data.data.content)})};l();const u=()=>{l()};return(r,d)=>{const _=o("icon-refresh"),s=o("a-space"),f=o("a-layout-footer");return y(),S(f,{class:"app-footer"},{default:n(()=>[e(s,null,{default:n(()=>[c("div",G,[t.value?j("",!0):(y(),B(F,{key:0},[C("\u6B63\u5728\u52A0\u8F7D\u4ECA\u65E5\u8BD7\u8BCD....")],64)),C(" "+k(t.value),1)]),e(_,{onClick:u,class:"app-refresh"})]),_:1})]),_:1})}}});const K=P(J,[["__scopeId","data-v-987e0ea2"]]);function O(g=!1){const t=p(g);return{loading:t,setLoading:r=>{t.value=r},toggle:()=>{t.value=!t.value}}}const W=U('<div class="app-login-top"><div class="header"><img src="'+$+'" alt="tiger" class="logo"><span class="title">Vue3-Tiger-Admin</span></div><div class="desc">\u4E00\u4E2A\u666E\u901A\u7684 Vue3 \u5F00\u53D1\u6A21\u677F</div></div>',1),X={class:"app-login"},Y={class:"login-form-error-msg"},Z={class:"app-login-footer",style:{display:"none111"}},se=V({__name:"index",setup(g){const{loading:t,setLoading:l}=O(),u=z(),r=p(""),d=L(),_=p(null),s=T({username:"admin",password:"123456",imgCode:"v9am"}),f=()=>{},w=()=>{x(_).validate(async a=>{if(a)return;await d.login(s);const{redirect:v,...m}=u.currentRoute.value.query;u.push({path:v||H,query:{...m}}),setTimeout(()=>{M.success("Hi\uFF0C\u6B22\u8FCE\u56DE\u6765")},500)}).catch(a=>{console.log("error",a)}).finally(()=>{l(!1)})};return(b,a)=>{const v=o("icon-user"),m=o("a-input"),h=o("a-form-item"),D=o("icon-lock"),E=o("a-input-password"),A=o("a-button"),N=o("a-space"),R=o("a-form");return y(),B(F,null,[W,c("div",X,[c("div",Y,k(r.value),1),e(R,{ref_key:"formRef",ref:_,model:s,class:"app-login-form",layout:"vertical"},{default:n(()=>[e(h,{field:"username",rules:[{required:!0,message:"\u8BF7\u8F93\u5165\u7528\u6237\u540D"}],"validate-trigger":["change","blur"],"hide-label":""},{default:n(()=>[e(m,{ref:"userNameInput",modelValue:s.username,"onUpdate:modelValue":a[0]||(a[0]=i=>s.username=i),size:"large",placeholder:"\u7528\u6237\u540D: admin"},{prefix:n(()=>[e(v)]),_:1},8,["modelValue"])]),_:1}),e(h,{field:"password",rules:[{required:!0,message:"\u8BF7\u8F93\u5165\u5BC6\u7801"}],"validate-trigger":["change","blur"],"hide-label":""},{default:n(()=>[e(E,{modelValue:s.password,"onUpdate:modelValue":a[1]||(a[1]=i=>s.password=i),size:"large",placeholder:"\u5BC6\u7801: 123456","allow-clear":""},{prefix:n(()=>[e(D)]),_:1},8,["modelValue"])]),_:1}),e(h,{name:"imgCode"},{default:n(()=>[e(m,{modelValue:s.imgCode,"onUpdate:modelValue":a[2]||(a[2]=i=>s.imgCode=i),class:"login-code-input",size:"large"},null,8,["modelValue"]),c("img",{class:"app-code-img",src:Q,alt:"",onClick:f})]),_:1}),e(N,{size:16,direction:"vertical",style:{"margin-top":"20px"}},{default:n(()=>[e(A,{type:"primary","html-type":"submit",onClick:w,long:"",loading:x(t)},{default:n(()=>[C("\u786E\u5B9A ")]),_:1},8,["loading"])]),_:1})]),_:1},8,["model"])]),c("div",Z,[e(K)])],64)}}});export{se as default};
