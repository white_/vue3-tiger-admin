import{_ as f}from"./index.vue_vue_type_style_index_0_lang.94782574.js";import{a as w}from"./index.e5846dc5.js";import{d as g,v as A,Z as D,_ as b,y as k,o as C,f as x,e as E,l as T,r as v,a as m,c as p,w as d,b as r}from"./index.e04da2f0.js";import"./index.5548e431.js";function S({value:s,tinymceId:u,width:e,height:c,toolbar:t,plugins:i,emit:o,BaseUrl:l}){function a(){window.tinymce.init({selector:`#${u}`,width:e,height:c,language:"zh_CN",body_class:"panel-body ",object_resizing:!1,toolbar:(t.length>0,t),menubar:!0,plugins:i,end_container_on_empty_block:!0,powerpaste_word_import:"clean",code_dialog_height:450,code_dialog_width:1e3,advlist_bullet_styles:"square",advlist_number_styles:"default",imagetools_cors_hosts:["www.tinymce.com","codepen.io"],default_link_target:"_blank",skin_url:l+"/tinymce4.7.5/skins/lightgray",link_title:!1,init_instance_callback:n=>{s&&n.setContent(s),n.on("NodeChange Change KeyUp SetContent",()=>{o("update:content",n.getContent())})}})}function F(){window.tinymce.get(u)&&window.tinymce.get(u).destroy()}function _(n){window.tinymce.get(u).setContent(n)}function B(){window.tinymce.get(u).getContent()}function h(n){n.forEach(y=>{window.tinymce.get(u).insertContent(`<img class="wscnph" src="${y.url}" >`)})}return{initTinymce:a,destroyTinymce:F,setContent:_,getContent:B,imageSuccessCBK:h}}const $={plugins:[`advlist anchor autolink autosave code codesample colorpicker colorpicker
    contextmenu directionality emoticons fullscreen hr image imagetools importcss insertdatetime
    legacyoutput link lists media nonbreaking noneditable pagebreak paste preview print save searchreplace
    spellchecker tabfocus table template textcolor textpattern visualblocks visualchars wordcount`],toolbar:[`bold italic underline strikethrough alignleft aligncenter
    alignright outdent indent  blockquote undo redo removeformat`,`hr bullist numlist link image charmap	 preview anchor pagebreak
      fullscreen insertdatetime media table forecolor backcolor`]},U={class:"tinymce-container editor-container"},N=["id"],K=E("div",{class:"editor-custom-btn-container"},null,-1),L=g({__name:"index",props:{id:String,width:String,height:String,value:String,disabled:Boolean,debug:Boolean},emits:["onClick","update:content"],setup(s,{emit:u}){const e=s,c=e.id||"vue-tinymce-"+ +new Date,{toolbar:t,plugins:i}=$,o="/vue3-tiger-admin/",l=o+"/tinymce4.7.5/tinymce.min.js?&t="+new Date().getTime(),a=S({...e,tinymceId:c,toolbar:t,plugins:i,BaseUrl:o,value:e.value,emit:u});return A(async()=>{const{load:F}=w(l,()=>{},{manual:!0});await F(),a.initTinymce()}),D(()=>{a.initTinymce()}),b(()=>{a.destroyTinymce()}),k(()=>{a.destroyTinymce()}),(F,_)=>(C(),x("div",U,[E("textarea",{id:T(c),class:"tinymce-textarea"},null,8,N),K]))}});const M=["innerHTML"],R=g({__name:"index",setup(s){const u=v(`
<h3>\u6625\u6C5F\u82B1\u6708\u591C</h3>
<h4>\u4F5C\u8005\uFF1A\u5F20\u82E5\u865A</h4>

<div>\u6625\u6C5F\u6F6E\u6C34\u8FDE\u6D77\u5E73\uFF0C\u6D77\u4E0A\u660E\u6708\u5171\u6F6E\u751F\u3002</div>
<div>\u6EDF\u6EDF\u968F\u6CE2\u5343\u4E07\u91CC\uFF0C\u4F55\u5904\u6625\u6C5F\u65E0\u6708\u660E\uFF01</div>
<div>\u6C5F\u6D41\u5B9B\u8F6C\u7ED5\u82B3\u7538\uFF0C\u6708\u7167\u82B1\u6797\u7686\u4F3C\u9730\uFF1B</div>
<div>\u7A7A\u91CC\u6D41\u971C\u4E0D\u89C9\u98DE\uFF0C\u6C40\u4E0A\u767D\u6C99\u770B\u4E0D\u89C1\u3002</div> 
<div>\u6C5F\u5929\u4E00\u8272\u65E0\u7EA4\u5C18\uFF0C\u768E\u768E\u7A7A\u4E2D\u5B64\u6708\u8F6E\u3002</div> 
<div>\u6C5F\u7554\u4F55\u4EBA\u521D\u89C1\u6708\uFF1F\u6C5F\u6708\u4F55\u5E74\u521D\u7167\u4EBA\uFF1F</div> 
<div>\u4EBA\u751F\u4EE3\u4EE3\u65E0\u7A77\u5DF2\uFF0C\u6C5F\u6708\u5E74\u5E74\u671B\u76F8\u4F3C\u3002</div>
<div>\u4E0D\u77E5\u6C5F\u6708\u5F85\u4F55\u4EBA\uFF0C\u4F46\u89C1\u957F\u6C5F\u9001\u6D41\u6C34\u3002</div>
<div>\u767D\u4E91\u4E00\u7247\u53BB\u60A0\u60A0\uFF0C\u9752\u67AB\u6D66\u4E0A\u4E0D\u80DC\u6101\u3002</div>
<div>\u8C01\u5BB6\u4ECA\u591C\u6241\u821F\u5B50\uFF1F\u4F55\u5904\u76F8\u601D\u660E\u6708\u697C\uFF1F</div>
<div>\u53EF\u601C\u697C\u4E0A\u6708\u5F98\u5F8A\uFF0C\u5E94\u7167\u79BB\u4EBA\u5986\u955C\u53F0\u3002</div>
<div>\u7389\u6237\u5E18\u4E2D\u5377\u4E0D\u53BB\uFF0C\u6363\u8863\u7827\u4E0A\u62C2\u8FD8\u6765\u3002</div>
<div>\u6B64\u65F6\u76F8\u671B\u4E0D\u76F8\u95FB\uFF0C\u613F\u9010\u6708\u534E\u6D41\u7167\u541B\u3002</div>
<div>\u9E3F\u96C1\u957F\u98DE\u5149\u4E0D\u5EA6\uFF0C\u9C7C\u9F99\u6F5C\u8DC3\u6C34\u6210\u6587\u3002</div>
<div>\u6628\u591C\u95F2\u6F6D\u68A6\u843D\u82B1\uFF0C\u53EF\u601C\u6625\u534A\u4E0D\u8FD8\u5BB6\u3002</div>
<div>\u6C5F\u6C34\u6D41\u6625\u53BB\u6B32\u5C3D\uFF0C\u6C5F\u6F6D\u843D\u6708\u590D\u897F\u659C\u3002</div>
<div>\u659C\u6708\u6C89\u6C89\u85CF\u6D77\u96FE\uFF0C\u78A3\u77F3\u6F47\u6E58\u65E0\u9650\u8DEF\u3002</div>
<div>\u4E0D\u77E5\u4E58\u6708\u51E0\u4EBA\u5F52\uFF0C\u843D\u6708\u6447\u60C5\u6EE1\u6C5F\u6811\u3002</div>
`),e=v(1);return D(()=>{e.value++}),(c,t)=>{const i=m("a-col"),o=m("a-row");return C(),p(f,{class:"app-text-editor",title:"\u5BCC\u6587\u672C\u7F16\u8F91\u5668"},{default:d(()=>[r(o,{gutter:24},{default:d(()=>[r(i,{span:12},{default:d(()=>[(C(),p(L,{key:e.value,content:u.value,"onUpdate:content":t[0]||(t[0]=l=>u.value=l),value:u.value,width:"100%",height:"440"},null,8,["content","value"]))]),_:1}),r(i,{span:12},{default:d(()=>[E("div",{class:"content",innerHTML:u.value},null,8,M)]),_:1})]),_:1})]),_:1})}}});export{R as default};
