import { createApp } from 'vue'
import ArcoVue from '@arco-design/web-vue'
import ArcoVueIcon from '@arco-design/web-vue/es/icon'

import App from './App.vue'
import router from './router'
import store from './store'
import '@/router/permission'

import 'uno.css'
import '@arco-design/web-vue/dist/arco.css'
import '@/styles/app.scss'
import 'animate.css'

const app = createApp(App)

app.use(ArcoVue)
app.use(ArcoVueIcon)
app.use(router)
app.use(store)

app.mount('#app')
